libvpd (2.2.9-1) unstable; urgency=medium

  * Update to new upstream version 2.2.9.
  * Update copyright details
  * Fix typo in symbols file's Build-Depends-Package
  * Import symbols file improvements from Ubuntu with lto
  * Follow SONAME change

 -- Frédéric Bonnard <frediz@debian.org>  Fri, 22 Apr 2022 13:05:19 +0200

libvpd (2.2.8-1) unstable; urgency=medium

  * New upstream release
  * Update d/watch to version 4
  * Update d/copyright year
  * Bump Standards-Version to 4.6.0.1
  * d/patches : Remove dynamic exception specification (Closes: #992074)

 -- Frédéric Bonnard <frediz@debian.org>  Fri, 01 Oct 2021 13:25:14 +0200

libvpd (2.2.7-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Frédéric Bonnard ]
  * Update to new upstream version 2.2.7.
  * Switch to debhelper-compat usage
  * List not installed files explicitly
  * Update d/copyright file with new version
  * Update homepage after github migration
  * Add Build-Depends-Package field to symbol file
  * Add Rules-Requires-Root field
  * Bump Standards-Version to 4.5.0

 -- Frédéric Bonnard <frediz@debian.org>  Fri, 31 Jul 2020 14:24:34 +0200

libvpd (2.2.6-1) unstable; urgency=medium

  * New upstream release
  * Handle special upstream version of tag for gbp
  * Rediff patches
  * Delete unneeded debian patch
  * Ignore void std::* symbols
  * Improve packaging author information
  * Update Vcs infos
  * Use https for the homepage
  * Upgrade debhelper compatibility level
  * Bump Standards-Version

 -- Frédéric Bonnard <frediz@debian.org>  Thu, 19 Jul 2018 16:58:06 +0200

libvpd (2.2.5-3) unstable; urgency=medium

  * d/p/0001-Changes-run.vpdupdate-creation-path-from-var-lib-lsv.patch
    Use runtime directory for the udev notify file, instead of
    /var/lib. Thanks Dimitri John Ledkov. (Closes: #872993)
  * d/control :
    - Fix accents in my name
  * d/control : upgrade Standard-version to 4.1.0
    - Use Priority optional instead of extra
    - d/copyright : using https for Format

 -- Frédéric Bonnard <frediz@linux.vnet.ibm.com>  Wed, 27 Sep 2017 15:06:21 +0000

libvpd (2.2.5-2) unstable; urgency=medium

  * Fix license LGPL-2.1+ and its content.
  * Improve symbol file for better compatibility with Ubuntu and -O3
    optimisation which generates or removes symbols (std::* not meant to be
    exported anyway) on ppc64el. Marking them optional.

 -- Frederic Bonnard <frediz@linux.vnet.ibm.com>  Thu, 16 Jun 2016 18:43:15 +0200

libvpd (2.2.5-1) unstable; urgency=medium

  * Import from Ubuntu (Closes: #740172)

 -- Frederic Bonnard <frediz@linux.vnet.ibm.com>  Wed, 01 Jun 2016 11:29:15 +0200
